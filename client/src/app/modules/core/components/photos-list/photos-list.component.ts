import {Component, OnInit} from '@angular/core';
import {PhotosService} from "../../../shared/services/photos.service";

@Component({
  selector: 'app-photos-list',
  templateUrl: './photos-list.component.html',
  styleUrls: ['./photos-list.component.scss']
})
export class PhotosListComponent implements OnInit {

  constructor(private photoService: PhotosService,) {
  }

  ngOnInit() {
  }

  viewPhoto(photoUrl: string) {
    this.photoService.photoEmitter.emit(
      photoUrl
    )
  }

}
