import {animate, state, style, transition, trigger} from "@angular/animations";
import {GALLERY_STATES} from "../../constants";

export const imageAnimations =
  [
    trigger('galleryState', [
      state(GALLERY_STATES.IDLE, style({
        filter: 'none',
      })),
      state(GALLERY_STATES.LOADING, style({
        filter: 'blur(25px)',
      })),
      transition(`${GALLERY_STATES.IDLE} => ${GALLERY_STATES.LOADING}`, animate('300ms ease-in')),
      transition(`${GALLERY_STATES.LOADING} => ${GALLERY_STATES.IDLE}`, animate('300ms ease-out'))
    ]),
  ];
