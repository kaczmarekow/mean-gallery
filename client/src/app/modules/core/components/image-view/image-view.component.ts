import {Component, Input, OnInit, Sanitizer} from '@angular/core';
import {GALLERY_MODES, GALLERY_STATES} from "../../constants";
import {SafeStyle} from "@angular/platform-browser";
import {LoadProgress, PhotosService} from "../../../shared/services/photos.service";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {imageAnimations} from "./image-view.animations";

@Component({
  selector: 'app-image-view',
  templateUrl: './image-view.component.html',
  styleUrls: ['./image-view.component.scss'],
  animations: imageAnimations
})
export class ImageViewComponent implements OnInit {

  mode: string = GALLERY_MODES.MODE_BACKGROUND;
  state: string = GALLERY_STATES.IDLE;
  loadProgress: string;

  @Input() imageUrl: string;
  imageResource: SafeStyle;
  oldImageUrl: string;

  constructor(private photosService: PhotosService,) {
  }

  loadPhoto(photoUrl: string) {

    this.state = GALLERY_STATES.LOADING;

    this.photosService.loadPhoto(photoUrl)
      .finally((data) => {
        console.log("FINALLY", data);
      })
      .subscribe(
        (data: LoadProgress) => {
          console.log("NEXY", data);
          if (data) {
            this.loadProgress = `${(data.loaded / data.total) * 100} %`;
            if (data.photoUrl) {
              this.imageResource = data.photoUrl;
              this.state = GALLERY_STATES.IDLE;
            }
          }
        },
        (error) => {
          console.log("ERROR", error);
        },
        (data) => {
          console.log("COMPLETE", data);
        },
      )
  }

  ngOnInit() {
    this.loadPhoto('http://localhost:3000/');
    this.photosService.photoEmitter.subscribe((event) => {
      this.loadPhoto(event);
    })
  }

}
