export const GALLERY_MODES = {
  MODE_BACKGROUND: 'MODE_BACKGROUND',
  MODE_GALLERY: 'MODE_GALLERY',
  MODE_SLIDESHOW: 'MODE_SLIDESHOW'
};

export const GALLERY_STATES = {
  IDLE: 'IDLE',
  LOADING: 'LOADING'
};
