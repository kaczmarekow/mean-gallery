import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImageViewComponent} from "./components";
import {SharedModule} from "../shared/shared.module";
import {PhotosService} from "../shared/services/photos.service";
import { PhotosListComponent } from './components/photos-list/photos-list.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
  ],
  declarations: [
    ImageViewComponent,
    PhotosListComponent,
  ],
  exports: [
    ImageViewComponent,
  ],
  providers: [
    PhotosService,
  ]
})
export class CoreModule {
}
