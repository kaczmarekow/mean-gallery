import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SafePipe} from "./pipes/safe.pipe";
import {PhotosService} from "./services/photos.service";

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    SafePipe,
  ],
  providers: [
    PhotosService,
  ],
  exports: [
    SafePipe,
  ]
})
export class SharedModule {

}
