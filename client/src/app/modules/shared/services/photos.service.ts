import {EventEmitter, Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/finally';

@Injectable()
export class PhotosService {

  photoEmitter: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  loadPhoto(url: string, options?) {
    return Observable.create(observer => {
      let req = new XMLHttpRequest();
      req.responseType = 'blob';
      req.onprogress = (event) => {
        let loadProgress: LoadProgress = {
          loaded: event.loaded,
          total: event.total
        };
        observer.next(loadProgress);
      };
      req.open('GET', url, true);
      req.send();
      req.onloadend = (event) => {
        let loadProgress: LoadProgress = {
          loaded: event.loaded,
          total: event.total,
          photoUrl: `url(${URL.createObjectURL(req.response)})`
        };
        observer.next(loadProgress);
        observer.next();
        observer.complete();
      };
    });
  }
}

export interface LoadProgress {
  loaded: number;
  total: number;
  photoUrl?: string;
}
