import {MatButtonModule} from '@angular/material';
import {NgModule} from "@angular/core";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
  imports: [
    MatButtonModule,
    BrowserAnimationsModule,
  ],
  exports: [
    MatButtonModule,
    BrowserAnimationsModule,
  ],
})
export class AppMaterialModule {
}
